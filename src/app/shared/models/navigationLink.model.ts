export interface NavigationLink {
    name: string;
    link: string;
    linkTitle: string;
    iconName: string;
}
