import { Component, OnInit, Input } from '@angular/core';
import { NavigationLink } from '../../shared/models/navigationLink.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Input() public navigation: NavigationLink[];

  constructor() { }

  ngOnInit() {
  }

}
