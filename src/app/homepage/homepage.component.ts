import { Component, OnInit } from '@angular/core';
import { NavigationLink } from '../shared/models/navigationLink.model';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  public navigation:NavigationLink[];

  constructor() { }

  ngOnInit() {
    this.navigation = [{
      name: 'Sign Up',
      link: '#',
      linkTitle: '',
      iconName: 'edit'
    },
    {
      name: 'Sail With Us',
      link: '#',
      linkTitle: '',
      iconName: 'yacht'
    }]
  }

}
